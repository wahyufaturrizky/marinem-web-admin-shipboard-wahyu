/*

Template: Jobber - Job Portal Online Jobs Search Template
Author: potenzaglobalsolutions.com
Design and Developed by: potenzaglobalsolutions.com

NOTE: This file contains all scripts for the actual Template.

*/

/*================================================
[  Table of contents  ]
================================================

:: Menu
:: Tooltip
:: Counter
:: Owl carousel
:: Slickslider
:: Magnific Popup
:: Datetimepicker
:: Select2
:: Range Slider
:: Countdown
:: Scrollbar
:: Back to top

======================================
[ End table content ]
======================================*/
//POTENZA var

(function ($) {
  "use strict";
  var POTENZA = {};

/*************************
  Predefined Variables
*************************/
  var $window = $(window),
    $document = $(document),
    $body = $('body'),
    $countdownTimer = $('.countdown'),
    $counter = $('.counter');
  //Check if function exists
  $.fn.exists = function () {
    return this.length > 0;
  };

    /*************************
      datetimepicker
  *************************/
  POTENZA.datetimepickers = function () {
    if ($('.datetimepickers').exists()) {
      $('#datetimepicker-01, #datetimepicker-02, #datetimepicker-03, #datetimepicker-04, #datetimepicker-05, #datetimepicker-06,#datetimepicker-07, #datetimepicker-08, #datetimepicker-09, #datetimepicker-10, #datetimepicker-11' ).datetimepicker({
        format: 'L'
      });
    }
  };



   $document.ready(function () {
      POTENZA.datetimepickers();
    });
})(jQuery);


$(document).ready(function(){
  $(".new-order-mobile .search-group input").click(function(){
    $(".order-form").fadeIn();
  });
});
$(document).ready(function(){
  $(".new-order-mobile .new-order-btn .btn").click(function(){
    $(".order-form").fadeIn();
  });
});

$(document).ready(function(){
  $(".cancel-btn").click(function(){
    $(".order-form").fadeOut();
  });
});

$(document).ready(function(){ 
  $(".tab-btn-list .towage-tab").click(function(){
    $(".towage-table").fadeIn();
  });
  $(".back-arrow").click(function(){
    $(".towage-table").fadeOut();
  });

  // $("#pills-movement-tab, #pills-vesselinfo-tab, #pills-agentinfo-tab, #pills-orderinfo-tab, #pills-tuginfo-tab, #pills-deployment-tab, #pills-agentinfo1-tab, #pills-orderinfo1-tab, #pills-manifest-tab, #pills-confirmation-tab").click(function(){
  //   $("#pills-tabContent1").fadeIn();
  // });
  // $(".back-arrow").click(function(){
  //   $("#pills-tabContent1").fadeOut();
  // });
});


$(document).ready(function() {
    $(".tab-btn-list li").click(function () {
        $(".tab-btn-list li").removeClass("active");
        $(this).addClass("active");     
    });

});

$(document).ready(function(){
  $(".mobile-launch-book .next-btn").click(function(){
    $(".mobile-manifest").fadeIn();
  });
  $(".mobile-manifest .back-arrow").click(function(){
    $(".mobile-manifest").fadeOut();
  });
  $(".passenger-btn-details").click(function(){
    $(".mobile-passenger-details").fadeIn();
  });
  $(".mobile-passenger-details .back-arrow").click(function(){
    $(".mobile-passenger-details").fadeOut();
  });
  $(".cargo-btn-details").click(function(){
    $(".mobile-cargo-details").fadeIn();
  });
  $(".mobile-cargo-details .back-arrow").click(function(){
    $(".mobile-cargo-details").fadeOut();
  });
  $(".mobile-manifest .next-btn").click(function(){
    $(".mobile-order-cinfirm").fadeIn();
  });
  $(".mobile-order-cinfirm .back-arrow").click(function(){
    $(".mobile-order-cinfirm").fadeOut();
  });
  $(".mobile-order-cinfirm .next-btn").click(function(){
    $(".mobile-payment-details").fadeIn();
  });
  $(".mobile-payment-details .back-arrow").click(function(){
    $(".mobile-payment-details").fadeOut();
  });
  $(".mobile-payment-details .next-btn").click(function(){
    $(".mobile-payment-status").fadeIn();
  });
  $(".mobile-payment-status .back-arrow").click(function(){
    $(".mobile-payment-status").fadeOut();
  });
});