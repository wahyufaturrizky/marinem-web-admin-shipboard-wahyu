import './App.css';
import AllRoutes from './components/common/allRoutes';
import { Provider } from 'react-redux';
import store from './components/redux/store';

function App() {
  return (
  <Provider store={store}>
    <AllRoutes/>
  </Provider>
  );
}

export default App;
