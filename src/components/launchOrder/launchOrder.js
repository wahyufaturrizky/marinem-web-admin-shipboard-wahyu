import React, { useState, useEffect } from 'react';

import { connect } from 'react-redux';

import DateTimePicker from 'react-datetime-picker';

import Tabs from 'react-bootstrap/Tabs';
import Tab from 'react-bootstrap/Tab';
import Accordion from 'react-bootstrap/Accordion';
import NavLink from 'react-bootstrap/NavLink';
import Card from 'react-bootstrap/Card';

import SelectSearch from 'react-select';

import { Link } from 'react-router-dom';

import Header from '../common/header';

const LaunchOrder = (props) => {
    return (
        <>
        <Header Link={"/launchOrder"}/>
        <Content {...props}/>
        </>
    );
}

const Content = (props) => {  
  const link = "/launchOrder";
  
  const [Index, SetIndex] = useState([]);
  var IndexArray = Index;
  var IndexTemp = IndexArray.length-1;
  
  const [key, setKey] = useState(1);
  const [MobileTabKey, SetMobileTabKey] = useState(1);
  
  const [ServiceRequestTimeValue, SetServiceRequestTimeValue] = useState(new Date());
  
  const [DateOfCallValue, SetDateOfCallValue] = useState(new Date());
  
  const [CompanyNameList, SetCompanyNameList] = useState([]);
  const [CompanyName, SetCompanyName] = useState("");
  
  const [ContactPersonList, SetContactPersonList] = useState([]);
  const [ContactPerson, SetContactPerson] = useState("");
  
  const [AccountList, SetAccountList] = useState([]);
  const [Account, SetAccount] = useState("");

  const [JobTypeList, SetJobTypeList] = useState([]);
  const [JobType, SetJobType] = useState("");

  const [LocationVesselList, SetLocationVesselList] = useState([]);
  const [LocationVesselNameFrom, SetLocationVesselNameFrom] = useState([]);
  const [LocationVesselNameTo, SetLocationVesselNameTo] = useState([]);
  
  const [LocationVesselNameMobileTabFrom, SetLocationVesselNameMobileTabFrom] = useState("");
  const [LocationVesselNameMobileTabTo, SetLocationVesselNameMobileTabTo] = useState("");
  
  const [CustomerContactData, SetCustomerContactData] = useState("");
  const [Email, SetEmail] = useState("");
  const [Phone, SetPhone] = useState("");
  const [Fax, SetFax] = useState("");
  
  const [DateValue, SetDateValue] = useState(new Date());
  const [ExpiryDateValue, SetExpiryDateValue] = useState(new Date());
  
  const DynamicContent = (props) => {
    return (
        <div className="manifest-wrapper">
        <div className="remove-panel">
		  <Link to={link} onClick={() => { IndexArray.splice(props.index, 1, null); IndexTemp=IndexArray.length-1; SetIndex(IndexArray); document.getElementById(props.index).remove(); }}>Remove</Link>
        </div>
        <form className="manifest-form">
          <div className="row">
            <div className="col-md-6">
              <div className="form-group">
                <label>From<span className="required">*</span></label>
                <SelectSearch 
                options={LocationVesselList}
                isSearchable={true}
                value={LocationVesselNameFrom[props.index]}
                onChange={(val) => { LocationVesselNameFrom[props.index] = val; SetLocationVesselNameFrom(LocationVesselNameFrom); }}
                placeholder="Choose your location or vessel name" 
                />
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-md-6">
              <div className="form-group">
                <label>To<span className="required">*</span></label>
                <SelectSearch 
                options={LocationVesselList}
                isSearchable={true}
                value={LocationVesselNameTo[props.index]}
                onChange={(val) => { LocationVesselNameTo[props.index] = val; SetLocationVesselNameTo(LocationVesselNameTo); }}
                placeholder="Choose your location or vessel name" 
                />
              </div>
            </div>
          </div>
        </form>
        <div className="accordion">
          <div className="card mb-0">
          <Accordion defaultActiveKey="0" className="accordion-group">
            <Card>
            <Card.Header>
              <Accordion.Toggle as={NavLink} variant="link" eventKey="0" style={{color: "white"}}>
                Passenger Detail
              </Accordion.Toggle>
            </Card.Header>
            
            <Accordion.Collapse eventKey="0">
            <Card.Body>
                <form>
                  <div className="row">
                    <div className="col-md-6">
                      <div className="form-group">
                        <label>No. Of Passenger</label>
                        <input type="text" className="form-control" id="nopass" />
                      </div>
                    </div>
                  </div>
                </form>
                <div className="passenger-btn">
                  <Link to={link} className="btn btn-default mr-2">Delete Passenger</Link>
                  <Link to={link} className="btn btn-primary">Add Passenger Profile</Link>
                </div>
            </Card.Body>
            </Accordion.Collapse>
            </Card>

            <Card>
            <Card.Header>
              <Accordion.Toggle as={NavLink} variant="link" eventKey="1" style={{color: "white"}}>
                Cargo Detail
              </Accordion.Toggle>
            </Card.Header>
            
            <Accordion.Collapse eventKey="1">
            <Card.Body>
                <form>
                  <div className="row">
                    <div className="col-md-6">
                      <div className="form-group">
                        <label>Commodity Type</label>
                        <select className="form-control" id="commodity">
                          <option>Search Commodity Type</option>
                          <option>Search Commodity Type1</option>
                          <option>Search Commodity Type2</option>
                          <option>Search Commodity Type3</option>
                          <option>Search Commodity Type4</option>
                        </select>
                      </div>
                    </div>
                    <div className="col-md-6">
                      <div className="form-group">
                        <label>Unit</label>
                        <select className="form-control" id="commodity">
                          <option>Unit</option>
                          <option>Unit1</option>
                          <option>Unit2</option>
                          <option>Unit3</option>
                          <option>Unit4</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-md-6">
                      <div className="form-group">
                        <label>Net Weight</label>
                        <select className="form-control" id="netweight">
                          <option>Net Weight</option>
                          <option>Net Weight1</option>
                          <option>Net Weight2</option>
                          <option>Net Weight3</option>
                          <option>Net Weight4</option>
                        </select>
                      </div>
                    </div>
                    <div className="col-md-6">
                      <div className="form-group">
                        <label>Gram Weight</label>
                        <select className="form-control" id="gramweight">
                          <option>Gram Weigh</option>
                          <option>Gram Weigh1</option>
                          <option>Gram Weigh2</option>
                          <option>Gram Weigh3</option>
                          <option>Gram Weigh4</option>
                        </select>
                      </div>
                    </div>
                  </div>
                </form>
            </Card.Body>
            </Accordion.Collapse>
            </Card>

            <Card>
            <Card.Header>
              <Accordion.Toggle as={NavLink} variant="link" eventKey="2" style={{color: "white"}}>
                File Attachment
              </Accordion.Toggle>
            </Card.Header>
            
            <Accordion.Collapse eventKey="2">
            <Card.Body>
                
            </Card.Body>
            </Accordion.Collapse>
            </Card>
          </Accordion>
          </div>
        </div>
      </div>
    );
}
  
  useEffect(() => {
  
    fetch('http://tenant1.innovez-one.com:8080/mrm/customer/list', 
    {
      method: 'GET',
      headers: {
        'Authorization': "Bearer " + props.AccessToken
      }
    })
    .then(response => {
      response.json().then(data1 => {
        var Arr = [];
        data1.content.aaData.forEach(element => {
          Arr.push({ value: element.id, label: element.name });  
        });

        Arr.sort(function(a, b) {
          var textA = a.label.toUpperCase();
          var textB = b.label.toUpperCase();
          return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
        });

        SetCompanyNameList(Arr);
      });
    })

    fetch('http://tenant1.innovez-one.com:8080/mrm/customer-contact/list', 
    {
      method: 'GET',
      headers: {
        'Authorization': "Bearer " + props.AccessToken
      }
    })
    .then(response => {
      response.json().then(data1 => {
		SetCustomerContactData(data1);
		
		var Arr = [];
        data1.content.aaData.forEach(element => {
          Arr.push({ value: element.id, label: element.id + " " + element.name });  
        });

        Arr.sort(function(a, b) {
          var textA = a.label.toUpperCase();
          var textB = b.label.toUpperCase();
          return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
        });

        SetContactPersonList(Arr);
      });
    })

    fetch('http://tenant1.innovez-one.com:8080/mrm/customer-account/list', 
    {
      method: 'GET',
      headers: {
        'Authorization': "Bearer " + props.AccessToken
      }
    })
    .then(response => {
      response.json().then(data1 => {
        var Arr = [];
        data1.content.aaData.forEach(element => {
          Arr.push({ value: element.id, label: element.number });  
        });

        Arr.sort(function(a, b) {
          var textA = a.label.toUpperCase();
          var textB = b.label.toUpperCase();
          return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
        });

        SetAccountList(Arr);
      });
    })

    fetch('http://tenant1.innovez-one.com:8080/mrm/job-type/list', 
    {
      method: 'GET',
      headers: {
        'Authorization': "Bearer " + props.AccessToken
      }
    })
    .then(response => {
      response.json().then(data1 => {
        var Arr = [];
        data1.content.aaData.forEach(element => {
          Arr.push({ value: element.id, label: element.name });  
        });

        Arr.sort(function(a, b) {
          var textA = a.label.toUpperCase();
          var textB = b.label.toUpperCase();
          return (textA < textB) ? -1 : (textA > textB) ? 1 : 0;
        });

        SetJobTypeList(Arr);
      });
    });

    Promise.all([
      fetch('http://tenant1.innovez-one.com:8080/mrm/location/list', 
      {
        method: 'get',
        headers: {
          'authorization': "bearer " + props.AccessToken
        }
      }),
      fetch('http://tenant1.innovez-one.com:8080/mrm/vessel/list', 
      {
        method: 'get',
        headers: {
          'authorization': "bearer " + props.AccessToken
        }
      })
    ]).then((data) => {
	  data[0].json().then((data1) => {
        data[1].json().then((data2) => {
          var arrr = [];
          var i;
          for(i=0; i<data1.content.aaData.length; i++)
          {
            arrr.push({ value: data1.content.aaData[i].id, label: data1.content.aaData[i].name });
          }

          for(i=0; i<data2.content.aaData.length; i++)
          {
            arrr.push({ value: data2.content.aaData[i].id, label: data2.content.aaData[i].name });
          }

          arrr.sort(function(a, b) {
            var texta = a.label.toUpperCase();
            var textb = b.label.toUpperCase();
            return (texta < textb) ? -1 : (texta > textb) ? 1 : 0;
          });
		  
          SetLocationVesselList(arrr);
        });
      });
    });
  }, [props.AccessToken]);

  useEffect(() => {
	if(CustomerContactData !== "")
	{
		for(var i=0; i<CustomerContactData.content.aaData.length; i++)
		{
			if(ContactPerson.value === CustomerContactData.content.aaData[i].id)
			{
				SetEmail(CustomerContactData.content.aaData[i].email);
				SetPhone(CustomerContactData.content.aaData[i].phone);
				SetFax(CustomerContactData.content.aaData[i].fax);
			}
		}
	}
  }, [ContactPerson, CustomerContactData]);
  
	if(props.AccessToken === "")
	{
		window.location.href = "/";
		
		return null;
	}
	else
	{
    return (
	<div className="main">
	
	<div className="nav nav-pills tab-menu">
		<Link to="/Console">
			<div className="nav-link">
				<img src="images/grid-icon.png" className="mr-2" alt=""/> Console
			</div>
		</Link>
	</div>
	
  <div className="main-wrapper desktop-launch-order">
    <h3 className="section-main-title">Create New Order</h3>
    <div className="tab-style-2">

    <Tabs activeKey={key} onSelect={(a) => setKey(a)} id="uncontrolled-tab-example" className="nav nav-pills tab-pills-menu">
      <Tab eventKey={1} title="Agent Information" className="nav-link">
      <div className="tab-content px-0 mb-0">
        <div className="tab-pane fade show active" id="pills-agentinfo1" role="tabpanel"
          aria-labelledby="pills-agentinfo1-tab">
          <div className="card-wrapper">
            <div className="tab-form">
              <form className="vessel-form">
                <div className="row">
                  <div className="col-md-6">
                    <div className="form-group">
                      <label>Company Name<span className="required">*</span></label>
                      <SelectSearch 
                      options={CompanyNameList}
                      isSearchable={true}
                      value={CompanyName}
                      onChange={(val) => SetCompanyName(val)}
                      placeholder="Choose your company name" 
                      />
                    </div>
                  </div>
                </div>

                <div className="row">
                  <div className="col-md-6">
                    <div className="form-group">
                      <label>Contact Person<span className="required">*</span></label>
                      <SelectSearch 
                      options={ContactPersonList}
                      isSearchable={true}
                      value={ContactPerson}
                      onChange={(val) => SetContactPerson(val)}
                      placeholder="Choose your contact person" 
                      />
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="form-group">
                      <label>Account<span className="required">*</span></label>
                      <SelectSearch 
                      options={AccountList}
                      isSearchable={true}
                      value={Account}
                      onChange={(val) => SetAccount(val)}
                      placeholder="Choose your account" 
                      />
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-6">
                    <div className="form-group">
                      <label>Email</label>
                      <input type="email" className="form-control" id="email" value={Email}/>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-6">
                    <div className="form-group">
                      <label>Phone</label>
                      <input type="text" className="form-control" id="phone" value={Phone}/>
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="form-group">
                      <label>Fax</label>
                      <input type="text" className="form-control" id="fax" value={Fax}/>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-6">
                    <div className="form-group">
                      <label>Contact Number</label>
                      <input type="text" className="form-control" id="conumber" />
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="form-group">
                      <label>Voyage No</label>
                      <input type="text" className="form-control" id="VoyageNo" />
                    </div>
                  </div>
                </div>
              </form>
            </div>
            <div className="tab-footer">
              <div className="btn-group">
                <div className="next-btn">
                  <button onClick={() => setKey(2)} className="btn btn-primary">Next<img src="images/next-arrow.svg" className="ml-2" alt="img" /></button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      </Tab>
      
      <Tab eventKey={2} title="Order Information" className="nav-link">
      <div className="tab-content px-0 mb-0">
        <div className="tab-pane fade show active" id="pills-orderinfo1" role="tabpanel" aria-labelledby="pills-orderinfo1-tab">
          <div className="card-wrapper">
            <div className="tab-form">
              <form className="order-form">
                <div className="row">
                  <div className="col-md-6">
                    <div className="form-group datetimepickers">
                      <label>Service Request Time<span className="required">*</span></label>
                      <div className="input-group">
                        <DateTimePicker className="form-control" onChange={(val) => SetServiceRequestTimeValue(val)} value={ServiceRequestTimeValue}/>
                      </div>
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="form-group datetimepickers">
                      <label>Date Of Call<span className="required">*</span></label>
                      <div className="input-group">
                        <DateTimePicker className="form-control" onChange={(val) => SetDateOfCallValue(val)} value={DateOfCallValue}/>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-6">
                    <div className="form-group">
                      <label>Type<span className="required">*</span></label>
                      <SelectSearch 
                      options={JobTypeList}
                      isSearchable={true}
                      value={JobType}
                      onChange={(val) => SetJobType(val)}
                      placeholder="Choose your job type" 
                      />
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="form-group">
                      <label>Media<span className="required">*</span></label>
                      <select className="form-control" id="vesselname">
                        <option>Phone</option>
                        <option>Fax</option>
                      </select>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-6">
                    <div className="form-group">
                      <label>Remark<span className="required">*</span></label>
                      <textarea className="form-control" id="remark" rows="4"></textarea>
                    </div>
                  </div>
                  <div className="col-md-6">
                    <div className="form-group">
                      <label>Bill Remark<span className="required">*</span></label>
                      <textarea className="form-control" id="remark" rows="4"></textarea>
                    </div>
                  </div>
                </div>
              </form>
            </div>
            <div className="tab-footer">
              <div className="btn-group">
                <div className="previous-btn mr-2">
                  <button onClick={() => setKey(1)} className="btn btn-primary-border"><img src="images/previous-arrow.svg" className="mr-2" alt="img" /> Previous</button>
                </div>
                <div className="next-btn">
                  <button onClick={() => setKey(3)} className="btn btn-primary">Next<img src="images/next-arrow.svg" className="ml-2" alt="img" /></button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      </Tab>

      <Tab eventKey={3} title="Manifest" className="nav-link">
      <div className="tab-content px-0 mb-0">
        <div className="tab-pane fade show active" id="pills-manifest" role="tabpanel" aria-labelledby="pills-manifest-tab">
          <div className="card-wrapper">
            <div className="tab-form manifest-form-wrapper">

				{
				Index.map(function(item, i){
					var component = React.createElement('div', { id:i }, <DynamicContent index={i} />)
					return component;
				})
				}
              
              <div className="d-flex">
                <Link to={link} onClick={() => { IndexArray.push(++IndexTemp); SetIndex(IndexArray); }} className="btn btn-primary btn-add"><img className="mr-2" src="images/add-icon.svg" alt="icon" />Add</Link>
              </div>
            </div>
            <div className="tab-footer">
              <div className="btn-group">
                <div className="previous-btn mr-2">
                  <button onClick={() => setKey(2)} className="btn btn-primary-border"><img src="images/previous-arrow.svg" className="mr-2" alt="img" />Previous</button>
                </div>
                <div className="next-btn">
                  <button onClick={() => setKey(4)} className="btn btn-primary">Next<img src="images/next-arrow.svg" className="ml-2" alt="img" /></button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      </Tab>
      
      <Tab eventKey={4} title="Confirmation" className="nav-link">
      <div className="tab-content px-0 mb-0">
        <div className="tab-pane fade show active" id="pills-confirmation" role="tabpanel" aria-labelledby="pills-confirmation-tab">
          <div className="card-wrapper">
            <div className="tab-form tab-form-wrapper">
              <div className="confirmation-wrapper">
                <div id="accordion2" className="accordion">
                  <div className="card mb-0">
                    <div className="accordion-group">
                      <div className="card-header collapsed" data-toggle="collapse" data-parent="#accordion2"
                        href="#agentcollapse">
                        <Link to={link} className="card-title">
                          Agent Information
                        </Link>
                      </div>
                      <div id="agentcollapse" className="collapse" data-parent="#accordion2">
                        <div className="card-body">
                        </div>
                      </div>
                    </div>
                    <div className="accordion-group">
                      <div className="card-header" data-toggle="collapse" data-parent="#accordion2"
                        href="#manifestcollapse">
                        <Link to={link} className="card-title">
                          Manifest
                        </Link>
                      </div>
                      <div id="manifestcollapse" className="card-body collapse show" data-parent="#accordion2">
                        <form>
                          <div className="row">
                            <div className="col-md-6">
                              <div className="form-data">
                                <div className="row">
                                  <div className="col-xl-4 col-lg-5 col-md-6">
                                    <label>Service Request Time :</label>
                                  </div>
                                  <div className="col-xl-8 col-lg-7 col-md-6">
                                    <span>23/10/2020 12:30</span>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="row">
                            <div className="col-md-6">
                              <div className="form-data">
                                <div className="row">
                                  <div className="col-xl-4 col-lg-5 col-md-6">
                                    <label>From :</label>
                                  </div>
                                  <div className="col-xl-8 col-lg-7 col-md-6">
                                    <span>West Coast Pier</span>
                                  </div>
                                </div>
                              </div>
                            </div>

                          </div>
                          <div className="row">
                            <div className="col-md-6">
                              <div className="form-data">
                                <div className="row">
                                  <div className="col-xl-4 col-lg-5 col-md-6">
                                    <label>To :</label>
                                  </div>
                                  <div className="col-xl-8 col-lg-7 col-md-6">
                                    <span>Vessel IB</span>
                                  </div>
                                </div>
                              </div>
                            </div>

                          </div>
                          <div className="row">
                            <div className="col-md-6">
                              <div className="form-data">
                                <div className="row">
                                  <div className="col-xl-4 col-lg-5 col-md-6">
                                    <label>No of Passengers :</label>
                                  </div>
                                  <div className="col-xl-8 col-lg-7 col-md-6">
                                    <span>3</span>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="row">
                            <div className="col-md-6">
                              <div className="form-data">
                                <div className="row">
                                  <div className="col-xl-4 col-lg-5 col-md-6">
                                    <label>No of Cargos :</label>
                                  </div>
                                  <div className="col-xl-8 col-lg-7 col-md-6">
                                    <span>2</span>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="row">
                            <div className="col-md-6">
                              <div className="form-data">
                                <div className="row">
                                  <div className="col-xl-4 col-lg-5 col-md-6">
                                    <label>Type :</label>
                                  </div>
                                  <div className="col-xl-8 col-lg-7 col-md-6">
                                    <span>One Way</span>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="row">
                            <div className="col-md-6">
                              <div className="form-data">
                                <div className="row">
                                  <div className="col-xl-4 col-lg-5 col-md-6">
                                    <label>Remark :</label>
                                  </div>
                                  <div className="col-xl-8 col-lg-7 col-md-6">
                                    <span></span>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                          <div className="row">
                            <div className="col-md-6">
                              <div className="form-data">
                                <div className="row">
                                  <div className="col-xl-4 col-lg-5 col-md-6">
                                    <label>Media :</label>
                                  </div>
                                  <div className="col-xl-8 col-lg-7 col-md-6">
                                    <span>Phone</span>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </div>
                        </form>
                      </div>
                    </div>

                  </div>
                </div>
              </div>
            </div>
            <div className="tab-footer">
              <div className="btn-group">
                <div className="previous-btn mr-2">
                  <button onClick={() => setKey(3)} href="#" className="btn btn-primary-border"><img src="images/previous-arrow.svg" className="mr-2" alt="img"/> Previous</button>
                </div>
                <div className="next-btn">
                  <button className="btn btn-primary">Save</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      </Tab>
    </Tabs>
    </div>
  </div>
  
  <div className="mobile-wrapper">
	
	<Tabs activeKey={MobileTabKey}>
    <Tab eventKey={1}>
	<div className="mobile-launch-book mobile-wrapper-inner">
		<div className="container">
			<h3 className="section-main-title">Book Launch</h3>
			
			<div className="map">
				<div className="embed-responsive embed-responsive-16by9">
						<iframe
						src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d255289.853490728!2d103.60605396471844!3d1.2266703479573262!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x31da1ae310c41683%3A0x5206b95f6246fcff!2sWest%20Coast%20Pier!5e0!3m2!1sen!2sin!4v1610976829746!5m2!1sen!2sin"
						frameBorder="0" style={{border: "0"}} allowFullScreen="" aria-hidden="false" tabIndex="0"></iframe>	
				</div>
			</div>
			
			<form className="form-wrapper book-launch-form">
				<div className="row">
					<div className="col-12">
						<div className="form-group">
							<label>Service Request Time</label>
							<select className="form-control" id="srequesttime">
								<option>Service Request Time</option>
								<option>Service Request Time</option>
								<option>Service Request Time</option>
								<option>Service Request Time</option>
								<option>Service Request Time</option>
							</select>
						</div>
					</div>
					<div className="col-12">
						<div className="form-group datetimepickers">
							<label>Date</label>
							<div className="input-group date" id="datetimepicker-05" data-target-input="nearest">
								<DateTimePicker className="form-control" format="dd-MM-y" onChange={(val) => SetDateValue(val)} value={DateValue}/>
							</div>
						</div>
					</div>
					<div className="col-12">
						<div className="form-group">
							<label>Launch Provider</label>
							<select className="form-control" id="launchprovider">
								<option>Launch Provider</option>
								<option>Launch Provider</option>
								<option>Launch Provider</option>
								<option>Launch Provider</option>
								<option>Launch Provider</option>
							</select>
						</div>
					</div>
					<div className="col-12 mobile-btn-wrapper text-center">
						<div className="next-btn">
							<Link to={link} onClick={() => SetMobileTabKey(2)} className="btn btn-primary">Next<img src="images/next-arrow.svg" className="ml-2" alt="img" /></Link>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
	</Tab>
	
	<Tab eventKey={2}>
	<div className="mobile-manifest mobile-wrapper-inner" style={{display: "block"}}>
		<div className="container">
			<ul>
				<li><Link to={link} onClick={() => SetMobileTabKey(1)} className="back-arrow"><img src="images/back-icon.svg" alt=""/></Link></li>
				<li><h3 className="section-main-title mb-0">Manifest</h3></li>
			</ul>
			
			<form className="form-wrapper manifest-form">
				<div className="row">
					<div className="col-12">
						<div className="form-group">
							<label>From</label>
							<SelectSearch 
							options={LocationVesselList}
							isSearchable={true}
							value={LocationVesselNameMobileTabFrom}
							onChange={(val) => { SetLocationVesselNameMobileTabFrom(val) }}
							placeholder="Choose your location or vessel name" 
							/>
						</div>
					</div>
					<div className="col-12">
						<div className="form-group">
							<label>To</label>
							<SelectSearch 
							options={LocationVesselList}
							isSearchable={true}
							value={LocationVesselNameMobileTabTo}
							onChange={(val) => { SetLocationVesselNameMobileTabTo(val) }}
							placeholder="Choose your location or vessel name" 
							/>
						</div>
					</div>
					
					<div className="col-12">
						<div className="form-group">
							<div className="from-control-btn">
								<a href="#" className="passenger-btn-details d-flex justify-content-between">
									<span>Passenger</span>
									<span><img src="images/arrow-right.svg" alt="icon"/></span>
								</a>
							</div>
						</div>
					</div>
					
					<div className="col-12">
						<div className="form-group">
							<div className="from-control-btn">
								<a href="#" className="cargo-btn-details d-flex justify-content-between">
									<span>Cargo Details</span>
									<span><img src="images/arrow-right.svg" alt="icon"/></span>
								</a>
							</div>
						</div>
					</div>
					
					<div className="col-12 mobile-btn-wrapper text-center">
						<div className="next-btn">
							<Link to={link} onClick={() => SetMobileTabKey(3)} className="btn btn-primary">Book</Link>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
	</Tab>
	
	<Tab eventKey={3}>
	<div className="mobile-passenger-details mobile-wrapper-inner" style={{display: "block"}}>
		<div className="container">
			<ul>
				<li><Link to={link} onClick={() => SetMobileTabKey(2)} className="back-arrow"><img src="images/back-icon.svg" alt="" /></Link></li>
				<li><h3 className="section-main-title mb-0">Passanger</h3></li>
			</ul>
			
			<form className="form-wrapper passenger-form">
				<div className="row">
					<div className="col-12">
						<div className="form-group">
							<label>Number of Passenger</label>
							<select className="form-control" id="launchprovider">
								<option>Select Passenger</option>
								<option>Select Passenger</option>
								<option>Select Passenger</option>
								<option>Select Passenger</option>
								<option>Select Passenger</option>
							</select>
						</div>
					</div>
					
					<div className="col-12">
						<div className="form-group">
							<label>First Name</label>
							<input type="text" className="form-control" id="pasfname" placeholder="" />
						</div>
					</div>
					
					<div className="col-12">
						<div className="form-group">
							<label>Last Name</label>
							<input type="text" className="form-control" id="paslname" placeholder="" />
						</div>
					</div>
					
					<div className="col-12">
						<div className="form-group">
							<label>Email Address</label>
							<input type="text" className="form-control" id="pasemail" placeholder="" />
						</div>
					</div>
					
					<div className="col-12">
						<div className="form-group">
							<label>Mobile Number</label>
							<input type="text" className="form-control" id="pasfnum" placeholder="" />
						</div>
					</div>
					
					<div className="col-12 mobile-btn-wrapper text-center">
						<div className="next-btn">
							<Link to={link} onClick={() => SetMobileTabKey(4)} className="btn btn-primary">Ok</Link>
						</div>
					</div>
					
				</div>
			</form>
			
		</div>
	</div>
	</Tab>
	
	<Tab eventKey={4}>
	<div className="mobile-cargo-details mobile-wrapper-inner" style={{display: "block"}}>
		<div className="container">
			<ul className="breadcrumbs">
				<li><Link to={link} onClick={() => SetMobileTabKey(3)} className="back-arrow"><img src="images/back-icon.svg" alt=""/></Link></li>
				<li><h3 className="section-main-title mb-0">Cargo Detail</h3></li>
			</ul>
			
			<form className="form-wrapper cargo-detail-form">
				<div className="row">
					<div className="col-12">
						<div className="form-group">
							<label>Commodity Type</label>
							<select className="form-control" id="mobcommodity">
								<option>Pallet by Quantity</option>
								<option>Pallet by Quantity</option>
								<option>Pallet by Quantity</option>
								<option>Pallet by Quantity</option>
								<option>Pallet by Quantity</option>
							</select>
						</div>
					</div>
					
					<div className="col-12">
						<div className="form-group">
							<label>Units</label>
							<select className="form-control" id="mobunits">
								<option>Units</option>
								<option>Units</option>
								<option>Units</option>
								<option>Units</option>
								<option>Units</option>
							</select>
						</div>
					</div>
					
					<div className="col-12">
						<div className="form-group">
							<label>Net Weight</label>
							<input type="text" className="form-control" id="mobnetweight" placeholder="" />
						</div>
					</div>
					
					<div className="col-12">
						<div className="form-group">
							<label>Handler Name</label>
							<input type="text" className="form-control" id="mobhandername" placeholder="" />
						</div>
					</div>
					
					<div className="col-12">
						<div className="form-group">
							<label>Handler Contact Number</label>
							<input type="text" className="form-control" id="mobhandernum" placeholder="" />
						</div>
					</div>
					
					<div className="col-12">
						<div className="form-group">
							<label>Handler Email</label>
							<input type="text" className="form-control" id="mobhanderemail" placeholder="" />
						</div>
					</div>
					
					<div className="col-12 mobile-btn-wrapper text-center">
						<div className="next-btn">
							<Link to={link} onClick={() => SetMobileTabKey(5)} className="btn btn-primary">Ok</Link>
						</div>
					</div>
					
				</div>
			</form>
			
		</div>
	</div>
	</Tab>
	
	<Tab eventKey={5}>
	<div className="mobile-order-cinfirm mobile-wrapper-inner" style={{display: "block"}}>
		<div className="container">
			<ul className="breadcrumbs">
				<li><Link to={link} onClick={() => SetMobileTabKey(4)} className="back-arrow"><img src="images/back-icon.svg" alt=""/></Link></li>
				<li><h3 className="section-main-title mb-0">Order Confirmation</h3></li>
			</ul>
		
			<div className="form-wrapper order-confirm-form">
				<div className="card-wrapper">
					<div className="row">
						<div className="col-12">
							<div className="order-value">
								<span className="label-name">Order:</span>
								<span className="label-name">L12345G</span>
							</div>
							
							<div className="order-value">
								<span className="label-name">From:</span>
								<span className="label-name">WEST COAST PIER</span>
							</div>
							
							<div className="order-value">
								<span className="label-name">To:</span>
								<span className="label-name">PMT APPOLLO ACE</span>
							</div>
							
							<div className="order-value">
								<span className="label-name">Time:</span>
								<span className="label-name">12:55 PM</span>
							</div>
							
							<div className="order-value">
								<span className="label-name">Menifest:</span>
								<span className="label-name">2 Passengers</span>
							</div>
						</div>
					</div>
				</div>
			</div>
			
			<h5 className="text-center">The price of the one-way trip is $200.</h5>
		
			<div className="mobile-btn-wrapper text-center">
				<div className="next-btn">
					<Link to={link} onClick={() => SetMobileTabKey(6)} className="btn btn-primary">Next</Link>
				</div>
			</div>
		
		</div>
	</div>
	</Tab>
	
	<Tab eventKey={6}>
	<div className="mobile-payment-details mobile-wrapper-inner" style={{display: "block"}}>
		<div className="container">
			<ul className="breadcrumbs">
				<li><Link to={link} onClick={() => SetMobileTabKey(5)} className="back-arrow"><img src="images/back-icon.svg" alt=""/></Link></li>
				<li><h3 className="section-main-title mb-0">Payment Details</h3></li>
			</ul>
			
			<form className="form-wrapper payment-detail-form">
				<div className="row">
					<div className="col-12">
						<div className="form-group">
							<label>Name on Card</label>
							<input type="text" className="form-control" id="mobcname" placeholder="" />
						</div>
					</div>
					
					<div className="col-12">
						<div className="form-group">
							<label>Card Number</label>
							<input type="text" className="form-control" id="mobcnum" placeholder="" />
						</div>
					</div>
					
					<div className="col-12">
						<div className="form-group">
							<label>Email Address</label>
							<input type="text" className="form-control" id="mobcemail" placeholder="" />
						</div>
					</div>
					
					<div className="col-12">
						<div className="form-group">
							<label>Expiry Date</label>
							<DateTimePicker className="form-control" format="dd-MM-y" onChange={(val) => SetExpiryDateValue(val)} value={ExpiryDateValue}/>
						</div>
					</div>
					
					<div className="col-12">
						<div className="form-group">
							<label>Security Code</label>
							<input type="text" className="form-control" id="mobsecurecode" placeholder="" />
						</div>
					</div>
					
					<div className="col-12 mobile-btn-wrapper text-center">
						<div className="next-btn">
							<Link to={link} onClick={() => SetMobileTabKey(7)} className="btn btn-primary">Book</Link>
						</div>
					</div>
				</div>
			</form>
			
		</div>		
	</div>
	</Tab>

	<Tab eventKey={7}>
	<div className="mobile-payment-status mobile-wrapper-inner" style={{display: "block"}}>
		<div className="container">
			<ul>
				<li><Link to={link} onClick={() => SetMobileTabKey(6)} className="back-arrow"><img src="images/back-icon.svg" alt=""/></Link></li>
				<li><h3 className="section-main-title mb-0">Payment Status</h3></li>
			</ul>
			
			<div className="form-wrapper payment-status-form">
				<div className="text-center">
					<div className="payment-sucess">
						<img src="images/successful.png" alt="image" />
						<h5>Payment Successful</h5>
						<p>An e-ticket has been sent to your registered email address</p>
						<div className="barcode-img">
							<img src="images/barcode.png" img="icon"/>
						</div>
					</div>
				</div>
				
				<div className="card-wrapper">
					<div className="row">
						<div className="col-12">
							<div className="order-value">
								<span className="label-name">Order:</span>
								<span className="label-value">L12345G</span>
							</div>
							
							<div className="order-value">
								<span className="label-name">From:</span>
								<span className="label-value">WEST COAST PIER</span>
							</div>
							
							<div className="order-value">
								<span className="label-name">To:</span>
								<span className="label-value">PMT APPOLLO ACE</span>
							</div>
							
							<div className="order-value">
								<span className="label-name">Date/Time:</span>
								<span className="label-value">10 DEC 2020 12:55 PM</span>
							</div>
							
							<div className="order-value">
								<span className="label-name">Time:</span>
								<span className="label-value">12:55 PM</span>
							</div>
							
							<div className="order-value mb-0">
								<span className="label-name">Menifest:</span>
								<span className="label-value">2 Passengers</span>
							</div>
						</div>
					</div>
				</div>
				
			</div>
			
		</div>
	</div>
	</Tab>
	</Tabs>
	
  </div>
  
  </div>
    );
	}
}

const mapStateToProps = (state) => {
	return {
		AccessToken: state.accessToken
	};
};
export default connect(mapStateToProps, null)(LaunchOrder);