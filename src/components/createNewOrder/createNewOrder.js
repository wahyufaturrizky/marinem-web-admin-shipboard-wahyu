import { connect } from 'react-redux';

import Link from 'react-router-dom/Link';

import Header from '../common/header';

const CreateNewOrder = (props) => {
	if(props.AccessToken === "")
	{
		window.location.href = "/";
		
		return null;
	}
	else
	{
    return (
    <>
        <Header/>
        <Content/>
    </>
    );
	}
}

const Content = () => {
    return (
    <div class="main-wrapper">
        <h3 class="section-main-title">Create New Order</h3>
        <div class="new-order-list-wrapper">
          <div class="order-list">
            <Link to="/console">
              <h5>Console</h5>
            </Link>
          </div>
          <div class="order-list">
            <Link to="/launchOrder">
              <h5>Launch Order</h5>
            </Link>
          </div>
        </div>
      </div>
    );
}

const mapStateToProps = (state) => {
	return {
		AccessToken: state.accessToken
	};
};

export default connect(mapStateToProps, null)(CreateNewOrder);