import React, { useState } from 'react';
import Dropdown from 'react-bootstrap/Dropdown';

import { connect } from 'react-redux';

import DateTimePicker from 'react-datetime-picker';

import Tabs from 'react-bootstrap/Tabs';
import Tab from 'react-bootstrap/Tab';

import { Link } from 'react-router-dom';
import WindowScroll from '../common/windowScroll';

import Header from '../common/header';

const link = "/console";

const menuToggle = React.forwardRef(({ children, onClick }, ref) => (
  <Link 
  to="/console" 
  className="dropdown-toggle" 
  ref={ref} 
  onClick={(e) => {
    e.preventDefault();
    onClick(e);
  }}>
    {children}
  </Link>
));

const Console = (props) => {
  return (
    <>
    <Header Link={"/console"}/>
    <Content {...props}/>
    </>
  );
}

const UserManager = () => {
	return (
		  <div className="col-md-3 console-wrapper">
            <div className="console-box">
              <Link to={link} onSelect={WindowScroll()}>
                <div className="console-icon">
                  <div className="icon-wrapper">
                    <img src="images/console-icon-1.png" alt="icon"/>
                  </div>
                </div>
                <div className="console-title">
                  <h5>User Manager</h5>
                </div>
              </Link>
            </div>
          </div>
		  );
}

const CustomerManager = () => {
	return (
		  <div className="col-md-3 console-wrapper">
            <div className="console-box">
              <Link to={link} onSelect={WindowScroll()}>
                <div className="console-icon">
                  <div className="icon-wrapper">
                    <img src="images/console-icon-2.png" alt="icon"/>
                  </div>
                </div>
                <div className="console-title">
                  <h5>Customer Manager</h5>
                </div>
              </Link>
            </div>
          </div>
	);
}

const DataManager = () => {
	return (
		  <div className="col-md-3 console-wrapper">
            <div className="console-box">
              <Link to={link} onSelect={WindowScroll()}>
                <div className="console-icon">
                  <div className="icon-wrapper">
                    <img src="images/console-icon-3.png" alt="icon"/>
                  </div>
                </div>
                <div className="console-title">
                  <h5>Data Manager</h5>
                </div>
              </Link>
            </div>
          </div>
	);
}

const TariffManager = () => {
	return (
		<div className="col-md-3 console-wrapper">
            <div className="console-box">
               <Link to={link} onSelect={WindowScroll()}>
                <div className="console-icon">
                  <div className="icon-wrapper">
                    <img src="images/console-icon-4.png" alt="icon"/>
                  </div>
                </div>
                <div className="console-title">
                  <h5>Tariff Manager</h5>
                </div>
              </Link>
            </div>
          </div>
	);
}

const BillingManager = () => {
	return (
		<div className="col-md-3 console-wrapper">
            <div className="console-box">
               <Link to={link} onSelect={WindowScroll()}>
                <div className="console-icon">
                  <div className="icon-wrapper">
                    <img src="images/console-icon-5.png" alt="icon"/>
                  </div>
                </div>
                <div className="console-title">
                  <h5>Billing Manager</h5>
                </div>
              </Link>
            </div>
          </div>
	);
}

const BillingAudit = () => {
	return (
		<div className="col-md-3 console-wrapper">
            <div className="console-box">
              <Link to={link} onSelect={WindowScroll()}>
                <div className="console-icon">
                  <div className="icon-wrapper">
                    <img src="images/console-icon-6.png" alt="icon"/>
                  </div>
                </div>
                <div className="console-title">
                  <h5>Billing Audit</h5>
                </div>
              </Link>
            </div>
          </div>
	);
}

const ServiceOrderManager = (props) => {
	return (
		<div className="col-md-3 console-wrapper">
            <div className="console-box">
               <Link to={link} onClick={() => { props.OpenRequestTab(2) }}>
                <div className="console-icon">
                  <div className="icon-wrapper">
                    <img src="images/console-icon-7.png" alt="icon"/>
                  </div>
                </div>
                <div className="console-title">
                  <h5>Service Order Manager</h5>
                </div>
              </Link>
            </div>
          </div>
	);
}

const VesselSchedule = () => {
	return (
		<div className="col-md-3 console-wrapper">
            <div className="console-box">
              <Link to={link} onSelect={WindowScroll()}>
                <div className="console-icon">
                  <div className="icon-wrapper">
                    <img src="images/console-icon-8.png" alt="icon"/>
                  </div>
                </div>
                <div className="console-title">
                  <h5>Vessel Schedule</h5>
                </div>
              </Link>
            </div>
          </div>
	);
}

const OperationAudit = () => {
	return (
		<div className="col-md-3 console-wrapper">
            <div className="console-box">
              <Link to={link} onSelect={WindowScroll()}>
                <div className="console-icon">
                  <div className="icon-wrapper">
                    <img src="images/console-icon-9.png" alt="icon"/>
                  </div>
                </div>
                <div className="console-title">
                  <h5>Operation Audit</h5>
                </div>
              </Link>
            </div>
          </div>
	);
}

const JobManager = () => {
	return (
		<div className="col-md-3 console-wrapper">
            <div className="console-box">
              <Link to={link} onSelect={WindowScroll()}>
                <div className="console-icon">
                  <div className="icon-wrapper">
                    <img src="images/console-icon-10.png" alt="icon"/>
                  </div>
                </div>
                <div className="console-title">
                  <h5>Job Manager</h5>
                </div>
              </Link>
            </div>
          </div>
	);
}

const SearchPanelForDesktop = (props) => {
	return (
		<div className="new-order">
          <div className="order-btn">
			<div className="dropdown" style={{marginRight: "20px"}}>
              <Dropdown>
                <Dropdown.Toggle as={menuToggle}>
					<div className="btn btn-primary">
						New Order
					</div>
                </Dropdown.Toggle>

                <Dropdown.Menu>
				  <Dropdown.Item><Link to={link}>Pilot Order</Link></Dropdown.Item>
                  <Dropdown.Item><Link to="/launchOrder">Launch Order</Link></Dropdown.Item>
				  <Dropdown.Item><Link to={link}>Tug Order</Link></Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>
            </div>
          </div>
          <div className="request-form">
            <form>
              <div className="form-row">
                <div className="form-group">
                  <label>Order No:</label>
                  <input type="text" className="form-control" placeholder="Search Keyword"/>
                </div>
                <div className="form-group">
                  <label>Vessel Name:</label>
                  <input type="text" className="form-control" placeholder="Search Keyword"/>
                </div>
                <div className="form-group datetimepickers">
                  <label>Start Date</label>
                  <div className="input-group">
                    <DateTimePicker className="form-control" format="dd-MM-y" onChange={(val) => props.SetStartDate(val)} value={props.StartDate}/>
                  </div>
                </div>
                <div className="form-group datetimepickers">
                  <label>End Date:</label>
                  <div className="input-group">
                    <DateTimePicker className="form-control" format="dd-MM-y" onChange={(val) => props.SetEndDate(val)} value={props.EndDate}/>
                  </div>
                </div>
                <div className="form-group">
                  <label>Status:</label>
                  <select id="selectstatus" className="form-control">
                    <option selected="">Select Status</option>
                    <option>01</option>
                    <option>02</option>
                    <option>03</option>
                  </select>
                </div>
              </div>
            </form>
          </div>
		</div>
	);
}

const SearchPanelForMobile = (props) => {
	return (
		<div className="new-order">
          <div className="request-form">
            <form>
              <div className="form-row">
                <div className="form-group">
                  <label>Order No:</label>
                  <input type="text" className="form-control" placeholder="Search Keyword"/>
                </div>
                <div className="form-group">
                  <label>Vessel Name:</label>
                  <input type="text" className="form-control" placeholder="Search Keyword"/>
                </div>
                <div className="form-group datetimepickers">
                  <label>Start Date</label>
                  <div className="input-group">
                    <DateTimePicker className="form-control" format="dd-MM-y" onChange={(val) => props.SetStartDate(val)} value={props.StartDate}/>
                  </div>
                </div>
                <div className="form-group datetimepickers">
                  <label>End Date:</label>
                  <div className="input-group">
                    <DateTimePicker className="form-control" format="dd-MM-y" onChange={(val) => props.SetEndDate(val)} value={props.EndDate}/>
                  </div>
                </div>
                <div className="form-group">
                  <label>Status:</label>
                  <select id="selectstatus" className="form-control">
                    <option selected="">Select Status</option>
                    <option>01</option>
                    <option>02</option>
                    <option>03</option>
                  </select>
                </div>
              </div>
            </form>
          </div>
        </div>
	);
}

const InnerHeaderForDesktop = (props) => {
	return (
	  <div className="tab-menu">
		<ul className="nav nav-pills">
			<li className="nav-item">
				<Link to={link} onClick={() => {props.OpenConsoleTab(1);}} className="nav-link"><img src="images/grid-icon.png" className="mr-2" alt=""/> Console</Link>
			</li>
			<li className="nav-item">
				<Link to={link} className="nav-link active d-flex align-items-center">Request</Link>
			</li>
			<li className="nav-item">
				<Link to={link} className="nav-link">Return</Link>
			</li>
		</ul>
	  </div>
	);
}

const InnerHeaderForMobile = (props) => {
	return (
	  <div className="tab-menu">
		<ul className="nav nav-pills">
			<li className="nav-item">
				<Link to={link} onClick={() => {props.OpenConsoleTab(1);}} className="nav-link"><img src="images/grid-icon.png" className="mr-2" alt=""/> Console</Link>
			</li>
			<li className="nav-item">
				<Link to={link} className="nav-link active d-flex align-items-center">Request</Link>
			</li>
			<li className="nav-item">
				<Link to={link} className="nav-link">Return</Link>
			</li>
			<li className="nav-item" style={{marginRight: "0px", marginLeft: "auto"}}>
				
			<div className="dropdown">
              <Dropdown>
                <Dropdown.Toggle as={menuToggle}>
					<Link to={link} className="nav-link">+</Link>
                </Dropdown.Toggle>

                <Dropdown.Menu>
				  <Dropdown.Item><Link to={link}>Pilot Order</Link></Dropdown.Item>
                  <Dropdown.Item><Link to="/launchOrder">Launch Order</Link></Dropdown.Item>
				  <Dropdown.Item><Link to={link}>Tug Order</Link></Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>
            </div>
			
			</li>
		</ul>
	  </div>
	);
}

const ConsoleTab = (props) => {
	return (
	  <>
	  <div className="tab-menu">
		<ul className="nav nav-pills">
			<li className="nav-item">
				<Link to={link} className="nav-link active d-flex align-items-center"><img src="images/grid-icon.png" className="mr-2" alt=""/> Console</Link>
			</li>			
		</ul>
	  </div>
	  <div id="myTabContent">
      <div className="tab-pane fade show active" id="console" role="tabpanel" aria-labelledby="console-tab">
      <div className="console-list">
        <div className="row">

			<UserManager />
			
			<CustomerManager />
          
			<DataManager />
			
			<TariffManager />
			
			<BillingManager />
			
			<BillingAudit />
          
			<ServiceOrderManager OpenRequestTab={props.OpenRequestTab} />
          
			<VesselSchedule />
			
			<OperationAudit />
          
			<JobManager />
          
        </div>
       </div>
      </div>
      </div>
	  </>
	);
}

const RequestTabDataForDesktop = () => {
	return (
		<div class="order-container">
              <table className="table table-striped">
                <thead>
                  <tr>
                    <th scope="col">Request Ref</th>
                    <th scope="col">Vessel Name</th>
                    <th scope="col">Pilot SRT</th>
                    <th scope="col">Pick Up</th>
                    <th scope="col">Let go</th>
                    <th scope="col">Tug Srt</th>
                    <th scope="col">Tug Type</th>
                    <th scope="col">Status</th>
                    <th scope="col">Action</th>
                    <th scope="col">History</th>
                  </tr>
                </thead>
                <tbody>
                  <tr className="align-middle">
                    <td>20G0000002</td>
                    <td>BW PANTHER</td>
                    <td>12/07/2020 15:50</td>
                    <td>AEBC</td>
                    <td>APRAM</td>
                    <td>12/07/2020 15:50</td>
                    <td>
                      <Link to={link} onSelect={WindowScroll()} className="btn btn-default"><span className="d-flex align-items-center"><img className="mr-2"src="images/tug-icon.svg" alt=""/>Tug Type</span></Link>
                    </td>
                    <td>
                      <Link to={link} onSelect={WindowScroll()} className="status-btn green-bg"><span className="d-flex align-items-center"><img className="mr-2"src="images/approve-icon.svg" alt=""/>Approve</span></Link>
                    </td>
                    <td>N/A</td>
                    <td>
                      <Link to={link} onSelect={WindowScroll()} className="btn btn-default"><span className="d-flex align-items-center"><img className="mr-2"src="images/history-icon.svg" alt=""/>History</span></Link>
                    </td>
                  </tr>
                  <tr className="align-middle">
                    <td>20G0000002</td>
                    <td>BW PANTHER</td>
                    <td>12/07/2020 15:50</td>
                    <td>AEBC</td>
                    <td>APRAM</td>
                    <td>12/07/2020 15:50</td>
                    <td>
                      <Link to={link} onSelect={WindowScroll()} className="btn btn-default"><span className="d-flex align-items-center"><img className="mr-2"src="images/tug-icon.svg" alt=""/>Tug Type</span></Link>
                    </td>
                    <td>
                      <Link to={link} onSelect={WindowScroll()} className="status-btn red-bg"><span className="d-flex align-items-center"><img className="mr-2"src="images/cross-icon.svg" alt=""/>Uncompleted</span></Link>
                    </td>
                    <td>N/A</td>
                    <td>
                      <Link to={link} onSelect={WindowScroll()} className="btn btn-default"><span className="d-flex align-items-center"><img className="mr-2"src="images/history-icon.svg" alt=""/>History</span></Link>
                    </td>
                  </tr>
                  <tr className="align-middle">
                    <td>20G0000002</td>
                    <td>BW PANTHER</td>
                    <td>12/07/2020 15:50</td>
                    <td>AEBC</td>
                    <td>APRAM</td>
                    <td>12/07/2020 15:50</td>
                    <td>
                      <Link to={link} onSelect={WindowScroll()} className="btn btn-default"><span className="d-flex align-items-center"><img className="mr-2"src="images/tug-icon.svg" alt=""/>Tug Type</span></Link>
                    </td>
                    <td>
                      <Link to={link} onSelect={WindowScroll()} className="status-btn green-bg"><span className="d-flex align-items-center"><img className="mr-2"src="images/approve-icon.svg" alt=""/>Approve</span></Link>
                    </td>
                    <td>N/A</td>
                    <td>
                      <Link to={link} onSelect={WindowScroll()} className="btn btn-default"><span className="d-flex align-items-center"><img className="mr-2"src="images/history-icon.svg" alt=""/>History</span></Link>
                    </td>
                  </tr>
                  <tr className="align-middle">
                    <td>20G0000002</td>
                    <td>BW PANTHER</td>
                    <td>12/07/2020 15:50</td>
                    <td>AEBC</td>
                    <td>APRAM</td>
                    <td>12/07/2020 15:50</td>
                    <td>
                      <Link to={link} onSelect={WindowScroll()} className="btn btn-default"><span className="d-flex align-items-center"><img className="mr-2"src="images/tug-icon.svg" alt=""/>Tug Type</span></Link>
                    </td>
                    <td>
                      <Link to={link} onSelect={WindowScroll()} className="status-btn red-bg" href="#"><span className="d-flex align-items-center"><img className="mr-2"src="images/cross-icon.svg" alt=""/>Uncompleted</span></Link>
                    </td>
                    <td>N/A</td>
                    <td>
                      <Link to={link} onSelect={WindowScroll()} className="btn btn-default" href="#"><span className="d-flex align-items-center"><img className="mr-2"src="images/history-icon.svg" alt=""/>History</span></Link>
                    </td>
                  </tr>
                  <tr className="align-middle">
                    <td>20G0000002</td>
                    <td>BW PANTHER</td>
                    <td>12/07/2020 15:50</td>
                    <td>AEBC</td>
                    <td>APRAM</td>
                    <td>12/07/2020 15:50</td>
                    <td>
                      <Link to={link} onSelect={WindowScroll()} className="btn btn-default"><span className="d-flex align-items-center"><img className="mr-2"src="images/tug-icon.svg" alt=""/>Tug Type</span></Link>
                    </td>
                    <td>
                      <Link to={link} onSelect={WindowScroll()} className="status-btn green-bg" href="#"><span className="d-flex align-items-center"><img className="mr-2"src="images/approve-icon.svg" alt=""/>Approve</span></Link>
                    </td>
                    <td>N/A</td>
                    <td>
                      <Link to={link} onSelect={WindowScroll()} className="btn btn-default" href="#"><span className="d-flex align-items-center"><img className="mr-2"src="images/history-icon.svg" alt=""/>History</span></Link>
                    </td>
                  </tr>
                  <tr className="align-middle">
                    <td>20G0000002</td>
                    <td>BW PANTHER</td>
                    <td>12/07/2020 15:50</td>
                    <td>AEBC</td>
                    <td>APRAM</td>
                    <td>12/07/2020 15:50</td>
                    <td>
                      <Link to={link} onSelect={WindowScroll()} className="btn btn-default" href="#"><span className="d-flex align-items-center"><img className="mr-2"src="images/tug-icon.svg" alt=""/>Tug Type</span></Link>
                    </td>
                    <td>
                      <Link to={link} onSelect={WindowScroll()} className="status-btn green-bg" href="#"><span className="d-flex align-items-center"><img className="mr-2"src="images/approve-icon.svg" alt=""/>Approve</span></Link>
                    </td>
                    <td>N/A</td>
                    <td>
                      <Link to={link} onSelect={WindowScroll()} className="btn btn-default" href="#"><span className="d-flex align-items-center"><img className="mr-2"src="images/history-icon.svg" alt=""/>History</span></Link>
                    </td>
                  </tr>
                  <tr className="align-middle">
                    <td>20G0000002</td>
                    <td>BW PANTHER</td>
                    <td>12/07/2020 15:50</td>
                    <td>AEBC</td>
                    <td>APRAM</td>
                    <td>12/07/2020 15:50</td>
                    <td>
                      <Link to={link} onSelect={WindowScroll()} className="btn btn-default" href="#"><span className="d-flex align-items-center"><img className="mr-2"src="images/tug-icon.svg" alt=""/>Tug Type</span></Link>
                    </td>
                    <td>
                      <Link to={link} onSelect={WindowScroll()} className="status-btn green-bg" href="#"><span className="d-flex align-items-center"><img className="mr-2"src="images/approve-icon.svg" alt=""/>Approve</span></Link>
                    </td>
                    <td>N/A</td>
                    <td>
                      <Link to={link} onSelect={WindowScroll()} className="btn btn-default" href="#"><span className="d-flex align-items-center"><img className="mr-2"src="images/history-icon.svg" alt=""/>History</span></Link>
                    </td>
                  </tr>  
                  <tr className="align-middle">
                    <td>20G0000002</td>
                    <td>BW PANTHER</td>
                    <td>12/07/2020 15:50</td>
                    <td>AEBC</td>
                    <td>APRAM</td>
                    <td>12/07/2020 15:50</td>
                    <td>
                      <Link to={link} onSelect={WindowScroll()} className="btn btn-default" href="#"><span className="d-flex align-items-center"><img className="mr-2"src="images/tug-icon.svg" alt=""/>Tug Type</span></Link>
                    </td>
                    <td>
                      <Link to={link} onSelect={WindowScroll()} className="status-btn red-bg" href="#"><span className="d-flex align-items-center"><img className="mr-2"src="images/cross-icon.svg" alt=""/>Uncompleted</span></Link>
                    </td>
                    <td>N/A</td>
                    <td>
                      <Link to={link} onSelect={WindowScroll()} className="btn btn-default" href="#"><span className="d-flex align-items-center"><img className="mr-2"src="images/history-icon.svg" alt=""/>History</span></Link>
                    </td>
                  </tr>
                  <tr className="align-middle">
                    <td>20G0000002</td>
                    <td>BW PANTHER</td>
                    <td>12/07/2020 15:50</td>
                    <td>AEBC</td>
                    <td>APRAM</td>
                    <td>12/07/2020 15:50</td>
                    <td>
                      <Link to={link} onSelect={WindowScroll()} className="btn btn-default" href="#"><span className="d-flex align-items-center"><img className="mr-2"src="images/tug-icon.svg" alt=""/>Tug Type</span></Link>
                    </td>
                    <td>
                      <Link to={link} onSelect={WindowScroll()} className="status-btn red-bg" href="#"><span className="d-flex align-items-center"><img className="mr-2"src="images/cross-icon.svg" alt=""/>Uncompleted</span></Link>
                    </td>
                    <td>N/A</td>
                    <td>
                      <Link to={link} onSelect={WindowScroll()} className="btn btn-default" href="#"><span className="d-flex align-items-center"><img className="mr-2"src="images/history-icon.svg" alt=""/>History</span></Link>
                    </td>
                  </tr>
                  <tr className="align-middle">
                    <td>20G0000002</td>
                    <td>BW PANTHER</td>
                    <td>12/07/2020 15:50</td>
                    <td>AEBC</td>
                    <td>APRAM</td>
                    <td>12/07/2020 15:50</td>
                    <td>
                      <Link to={link} onSelect={WindowScroll()} className="btn btn-default" href="#"><span className="d-flex align-items-center"><img className="mr-2"src="images/tug-icon.svg" alt=""/>Tug Type</span></Link>
                    </td>
                    <td>
                      <Link to={link} onSelect={WindowScroll()} className="status-btn green-bg" href="#"><span className="d-flex align-items-center"><img className="mr-2"src="images/approve-icon.svg" alt=""/>Approve</span></Link>
                    </td>
                    <td>N/A</td>
                    <td>
                      <Link to={link} onSelect={WindowScroll()} className="btn btn-default" href="#"><span className="d-flex align-items-center"><img className="mr-2"src="images/history-icon.svg" alt=""/>History</span></Link>
                    </td>
                  </tr>
                  <tr className="align-middle">
                    <td>20G0000002</td>
                    <td>BW PANTHER</td>
                    <td>12/07/2020 15:50</td>
                    <td>AEBC</td>
                    <td>APRAM</td>
                    <td>12/07/2020 15:50</td>
                    <td>
                      <Link to={link} onSelect={WindowScroll()} className="btn btn-default" href="#"><span className="d-flex align-items-center"><img className="mr-2"src="images/tug-icon.svg" alt=""/>Tug Type</span></Link>
                    </td>
                    <td>
                      <Link to={link} onSelect={WindowScroll()} className="status-btn red-bg" href="#"><span className="d-flex align-items-center"><img className="mr-2"src="images/cross-icon.svg" alt=""/>Uncompleted</span></Link>
                    </td>
                    <td>N/A</td>
                    <td>
                      <Link to={link} onSelect={WindowScroll()} className="btn btn-default" href="#"><span className="d-flex align-items-center"><img className="mr-2"src="images/history-icon.svg" alt=""/>History</span></Link>
                    </td>
                  </tr>
                </tbody>
               </table>
			   </div>
	);
}

const RequestTabDataForMobile = () => {
	return (
		<>
		<div className="table-data-details">
                <div className="table-data-header">
                  <div className="data-heading-left">
                    <span>BW PANTHER</span>
                  </div>
                  <div className="data-heading-right">
                    <span>20G0000002</span>
                  </div>
                </div>
                <div className="table-data-body">
                  <div className="data-row row">
                    <div className="col-6 col-lg-2 col-md-3">
                      <div className="data-title">
                        <h6>Pilot SRT</h6>
                      </div>
                      <div className="data-wrapper">
                        <h5>12/07/2020 15:50</h5>
                      </div>
                    </div>
                    <div className="col-6 col-lg-2 col-md-3">
                      <div className="data-title">
                        <h6>Pick Up</h6>
                      </div>
                      <div className="data-wrapper">
                        <h5>AEBC</h5>
                      </div>
                    </div>
                    <div className="col-6 col-lg-2 col-md-3">
                      <div className="data-title">
                        <h6>Let Go</h6>
                      </div>
                      <div className="data-wrapper">
                        <h5>APRAM</h5>
                      </div>
                    </div>
                    <div className="col-6 col-lg-2 col-md-3">
                      <div className="data-title">
                        <h6>Tug Srt</h6>
                      </div>
                      <div className="data-wrapper">
                        <h5>12/07/2020 15:50</h5>
                      </div>
                    </div>
                    <div className="col-6 col-lg-2 col-md-3">
                      <div className="data-title">
                        <h6>Tug Type</h6>
                      </div>
                      <div className="data-wrapper">
                        <Link to={link} onSelect={WindowScroll()} className="btn btn-default"><span className="d-flex align-items-center"><img className="mr-2" src="images/tug-icon.svg" alt=""/>Tug Type</span></Link>
                      </div>
                    </div>
                    <div className="col-6 col-lg-2 col-md-3">
                      <div className="data-title">
                        <h6>Status</h6>
                      </div>
                      <div className="data-wrapper">
                        <Link to={link} onSelect={WindowScroll()} className="status-btn green-bg"><span className="d-flex align-items-center"><img className="mr-2" src="images/approve-icon.svg" alt=""/>Approve</span></Link>
                      </div>
                    </div>
                    <div className="col-6 col-lg-2 col-md-3">
                      <div className="data-title">
                        <h6>Action</h6>
                      </div>
                      <div className="data-wrapper">
                        <h5>N/A</h5>
                      </div>
                    </div>
                    <div className="col-6 col-md-2 col-sm-3">
                      <div className="data-title">
                        <h6>History</h6>
                      </div>
                      <div className="data-wrapper">
                        <Link to={link} onSelect={WindowScroll()} className="btn btn-default"><span className="d-flex align-items-center"><img className="mr-2" src="images/history-icon.svg" alt=""/>History</span></Link>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="table-data-details">
                <div className="table-data-header">
                  <div className="data-heading-left">
                    <span>BW PANTHER</span>
                  </div>
                  <div className="data-heading-right">
                    <span>20G0000002</span>
                  </div>
                </div>
                <div className="table-data-body">
                  <div className="data-row row">
                    <div className="col-6 col-lg-2 col-md-3">
                      <div className="data-title">
                        <h6>Pilot SRT</h6>
                      </div>
                      <div className="data-wrapper">
                        <h5>12/07/2020 15:50</h5>
                      </div>
                    </div>
                    <div className="col-6 col-lg-2 col-md-3">
                      <div className="data-title">
                        <h6>Pick Up</h6>
                      </div>
                      <div className="data-wrapper">
                        <h5>AEBC</h5>
                      </div>
                    </div>
                    <div className="col-6 col-lg-2 col-md-3">
                      <div className="data-title">
                        <h6>Let Go</h6>
                      </div>
                      <div className="data-wrapper">
                        <h5>APRAM</h5>
                      </div>
                    </div>
                    <div className="col-6 col-lg-2 col-md-3">
                      <div className="data-title">
                        <h6>Tug Srt</h6>
                      </div>
                      <div className="data-wrapper">
                        <h5>12/07/2020 15:50</h5>
                      </div>
                    </div>
                    <div className="col-6 col-lg-2 col-md-3">
                      <div className="data-title">
                        <h6>Tug Type</h6>
                      </div>
                      <div className="data-wrapper">
                        <Link to={link} onSelect={WindowScroll()} className="btn btn-default"><span className="d-flex align-items-center"><img className="mr-2" src="images/tug-icon.svg" alt=""/>Tug Type</span></Link>
                      </div>
                    </div>
                    <div className="col-6 col-lg-2 col-md-3">
                      <div className="data-title">
                        <h6>Status</h6>
                      </div>
                      <div className="data-wrapper">
                        <Link to={link} onSelect={WindowScroll()} className="status-btn green-bg"><span className="d-flex align-items-center"><img className="mr-2" src="images/approve-icon.svg" alt=""/>Approve</span></Link>
                      </div>
                    </div>
                    <div className="col-6 col-lg-2 col-md-3">
                      <div className="data-title">
                        <h6>Action</h6>
                      </div>
                      <div className="data-wrapper">
                        <h5>N/A</h5>
                      </div>
                    </div>
                    <div className="col-6 col-md-2 col-sm-3">
                      <div className="data-title">
                        <h6>History</h6>
                      </div>
                      <div className="data-wrapper">
                        <Link to={link} onSelect={WindowScroll()} className="btn btn-default"><span className="d-flex align-items-center"><img className="mr-2" src="images/history-icon.svg" alt=""/>History</span></Link>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="table-data-details">
                <div className="table-data-header">
                  <div className="data-heading-left">
                    <span>BW PANTHER</span>
                  </div>
                  <div className="data-heading-right">
                    <span>20G0000002</span>
                  </div>
                </div>
                <div className="table-data-body">
                  <div className="data-row row">
                    <div className="col-6 col-lg-2 col-md-3">
                      <div className="data-title">
                        <h6>Pilot SRT</h6>
                      </div>
                      <div className="data-wrapper">
                        <h5>12/07/2020 15:50</h5>
                      </div>
                    </div>
                    <div className="col-6 col-lg-2 col-md-3">
                      <div className="data-title">
                        <h6>Pick Up</h6>
                      </div>
                      <div className="data-wrapper">
                        <h5>AEBC</h5>
                      </div>
                    </div>
                    <div className="col-6 col-lg-2 col-md-3">
                      <div className="data-title">
                        <h6>Let Go</h6>
                      </div>
                      <div className="data-wrapper">
                        <h5>APRAM</h5>
                      </div>
                    </div>
                    <div className="col-6 col-lg-2 col-md-3">
                      <div className="data-title">
                        <h6>Tug Srt</h6>
                      </div>
                      <div className="data-wrapper">
                        <h5>12/07/2020 15:50</h5>
                      </div>
                    </div>
                    <div className="col-6 col-lg-2 col-md-3">
                      <div className="data-title">
                        <h6>Tug Type</h6>
                      </div>
                      <div className="data-wrapper">
                        <Link to={link} onSelect={WindowScroll()} className="btn btn-default"><span className="d-flex align-items-center"><img className="mr-2" src="images/tug-icon.svg" alt=""/>Tug Type</span></Link>
                      </div>
                    </div>
                    <div className="col-6 col-lg-2 col-md-3">
                      <div className="data-title">
                        <h6>Status</h6>
                      </div>
                      <div className="data-wrapper">
                        <Link to={link} onSelect={WindowScroll()} className="status-btn green-bg"><span className="d-flex align-items-center"><img className="mr-2" src="images/approve-icon.svg" alt=""/>Approve</span></Link>
                      </div>
                    </div>
                    <div className="col-6 col-lg-2 col-md-3">
                      <div className="data-title">
                        <h6>Action</h6>
                      </div>
                      <div className="data-wrapper">
                        <h5>N/A</h5>
                      </div>
                    </div>
                    <div className="col-6 col-md-2 col-sm-3">
                      <div className="data-title">
                        <h6>History</h6>
                      </div>
                      <div className="data-wrapper">
                        <Link to={link} onSelect={WindowScroll()} className="btn btn-default"><span className="d-flex align-items-center"><img className="mr-2" src="images/history-icon.svg" alt=""/>History</span></Link>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="table-data-details">
                <div className="table-data-header">
                  <div className="data-heading-left">
                    <span>BW PANTHER</span>
                  </div>
                  <div className="data-heading-right">
                    <span>20G0000002</span>
                  </div>
                </div>
                <div className="table-data-body">
                  <div className="data-row row">
                    <div className="col-6 col-lg-2 col-md-3">
                      <div className="data-title">
                        <h6>Pilot SRT</h6>
                      </div>
                      <div className="data-wrapper">
                        <h5>12/07/2020 15:50</h5>
                      </div>
                    </div>
                    <div className="col-6 col-lg-2 col-md-3">
                      <div className="data-title">
                        <h6>Pick Up</h6>
                      </div>
                      <div className="data-wrapper">
                        <h5>AEBC</h5>
                      </div>
                    </div>
                    <div className="col-6 col-lg-2 col-md-3">
                      <div className="data-title">
                        <h6>Let Go</h6>
                      </div>
                      <div className="data-wrapper">
                        <h5>APRAM</h5>
                      </div>
                    </div>
                    <div className="col-6 col-lg-2 col-md-3">
                      <div className="data-title">
                        <h6>Tug Srt</h6>
                      </div>
                      <div className="data-wrapper">
                        <h5>12/07/2020 15:50</h5>
                      </div>
                    </div>
                    <div className="col-6 col-lg-2 col-md-3">
                      <div className="data-title">
                        <h6>Tug Type</h6>
                      </div>
                      <div className="data-wrapper">
                        <Link to={link} onSelect={WindowScroll()} className="btn btn-default"><span className="d-flex align-items-center"><img className="mr-2" src="images/tug-icon.svg" alt=""/>Tug Type</span></Link>
                      </div>
                    </div>
                    <div className="col-6 col-lg-2 col-md-3">
                      <div className="data-title">
                        <h6>Status</h6>
                      </div>
                      <div className="data-wrapper">
                        <Link to={link} onSelect={WindowScroll()} className="status-btn green-bg"><span className="d-flex align-items-center"><img className="mr-2" src="images/approve-icon.svg" alt=""/>Approve</span></Link>
                      </div>
                    </div>
                    <div className="col-6 col-lg-2 col-md-3">
                      <div className="data-title">
                        <h6>Action</h6>
                      </div>
                      <div className="data-wrapper">
                        <h5>N/A</h5>
                      </div>
                    </div>
                    <div className="col-6 col-md-2 col-sm-3">
                      <div className="data-title">
                        <h6>History</h6>
                      </div>
                      <div className="data-wrapper">
                        <Link to={link} onSelect={WindowScroll()} className="btn btn-default"><span className="d-flex align-items-center"><img className="mr-2" src="images/history-icon.svg" alt=""/>History</span></Link>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
              <div className="table-data-details">
                <div className="table-data-header">
                  <div className="data-heading-left">
                    <span>BW PANTHER</span>
                  </div>
                  <div className="data-heading-right">
                    <span>20G0000002</span>
                  </div>
                </div>
                <div className="table-data-body">
                  <div className="data-row row">
                    <div className="col-6 col-lg-2 col-md-3">
                      <div className="data-title">
                        <h6>Pilot SRT</h6>
                      </div>
                      <div className="data-wrapper">
                        <h5>12/07/2020 15:50</h5>
                      </div>
                    </div>
                    <div className="col-6 col-lg-2 col-md-3">
                      <div className="data-title">
                        <h6>Pick Up</h6>
                      </div>
                      <div className="data-wrapper">
                        <h5>AEBC</h5>
                      </div>
                    </div>
                    <div className="col-6 col-lg-2 col-md-3">
                      <div className="data-title">
                        <h6>Let Go</h6>
                      </div>
                      <div className="data-wrapper">
                        <h5>APRAM</h5>
                      </div>
                    </div>
                    <div className="col-6 col-lg-2 col-md-3">
                      <div className="data-title">
                        <h6>Tug Srt</h6>
                      </div>
                      <div className="data-wrapper">
                        <h5>12/07/2020 15:50</h5>
                      </div>
                    </div>
                    <div className="col-6 col-lg-2 col-md-3">
                      <div className="data-title">
                        <h6>Tug Type</h6>
                      </div>
                      <div className="data-wrapper">
                        <Link to={link} onSelect={WindowScroll()} className="btn btn-default"><span className="d-flex align-items-center"><img className="mr-2" src="images/tug-icon.svg" alt=""/>Tug Type</span></Link>
                      </div>
                    </div>
                    <div className="col-6 col-lg-2 col-md-3">
                      <div className="data-title">
                        <h6>Status</h6>
                      </div>
                      <div className="data-wrapper">
                        <Link to={link} onSelect={WindowScroll()} className="status-btn green-bg"><span className="d-flex align-items-center"><img className="mr-2" src="images/approve-icon.svg" alt=""/>Approve</span></Link>
                      </div>
                    </div>
                    <div className="col-6 col-lg-2 col-md-3">
                      <div className="data-title">
                        <h6>Action</h6>
                      </div>
                      <div className="data-wrapper">
                        <h5>N/A</h5>
                      </div>
                    </div>
                    <div className="col-6 col-md-2 col-sm-3">
                      <div className="data-title">
                        <h6>History</h6>
                      </div>
                      <div className="data-wrapper">
                        <Link to={link} onSelect={WindowScroll()} className="btn btn-default"><span className="d-flex align-items-center"><img className="mr-2" src="images/history-icon.svg" alt=""/>History</span></Link>
                      </div>
                    </div>
                  </div>
				  </div>
				  </div>
				  </>
	);
}

const RequestTab = (props) => {
	return (
		<div id="myTabContent">
      <div className="tab-pane fade show active">
      <div className="request-content">
		<div className="new-order-mobile">
          <div className="order-wrapper d-flex align-items-center">
            <div className="new-order-btn mr-2">
              <Link to={link} onSelect={WindowScroll()} className="btn btn-primary"><img width="22px" src="images/add-icon.svg" alt="icon"/></Link>
            </div>
            <form>
              <div className="form-group mb-0">
                <div className="search-group">
                  <img className="search-icon" src="images/search-icon.svg" alt=""/>
                  <input type="text" className="form-control" placeholder="Search Here"/>
                </div>
              </div>
            </form>
          </div>
          <div className="order-form">
            <div className="request-form">
              <div className="search-header d-flex">
                <Link to={link} onSelect={WindowScroll()} className="cancel-btn">Cancel</Link>
                <h5 className="mb-0">Search</h5>
                <Link to={link} onSelect={WindowScroll()}>Reset</Link>
              </div>
              <form>
                <div className="form-row">
                  <div className="form-group">
                    <label>Order No:</label>
                    <input type="text" className="form-control" placeholder="Search Keyword"/>
                  </div>
                  <div className="form-group">
                    <label>Vessel Name:</label>
                    <input type="text" className="form-control" placeholder="Search Keyword"/>
                  </div>
                  <div className="form-group datetimepickers">
                    <label>Start Date</label>
                    <div className="input-group date" id="datetimepicker-03" data-target-input="nearest">
                      <input type="text" className="form-control datetimepicker-input" placeholder="05-06-2020" data-target="#datetimepicker-03"/>
                      <div className="input-group-append" data-target="#datetimepicker-03" data-toggle="datetimepicker">
                        <div className="input-group-text"><img src="images/calendar-full.svg" alt="calender"/></div>
                      </div>
                    </div>
                  </div>
                  <div className="form-group datetimepickers">
                    <label>End Date:</label>
                    <div className="input-group date" id="datetimepicker-04" data-target-input="nearest">
                      <input type="text" className="form-control datetimepicker-input" placeholder="15-06-2020" data-target="#datetimepicker-04"/>
                      <div className="input-group-append" data-target="#datetimepicker-04" data-toggle="datetimepicker">
                        <div className="input-group-text"><img src="images/calendar-full.svg" alt="calender"/></div>
                      </div>
                    </div>
                  </div>
                  <div className="form-group">
                    <label>Status:</label>
                    <select id="selectstatus" className="form-control">
                      <option selected="">Select Status</option>
                      <option>01</option>
                      <option>02</option>
                      <option>03</option>
                    </select>
                  </div>
                </div>
                <div className="apply-btn">
                  <Link to={link} className="btn btn-primary">Apply</Link>
                </div>
              </form>
          </div>
          </div>
        </div>
		
        <div className="bg-white">
			<div className="table-responsive request-table desktop-table">
			
		<InnerHeaderForDesktop OpenConsoleTab={props.OpenConsoleTab}/>
		
		<SearchPanelForDesktop SetStartDate={props.SetStartDate} SetEndDate={props.SetEndDate} StartDate={props.StartDate} EndDate={props.EndDate} />
		
		<RequestTabDataForDesktop />
			
            </div>
            <nav className="pagination-wrapper" aria-label="Page navigation ">
              <ul className="pagination">
                <li className="page-item">
                  <Link to={link} onSelect={WindowScroll()} className="page-link" aria-label="Previous">
                    <span aria-hidden="true">«</span>
                    <span className="sr-only">Previous</span>
                  </Link>
                </li>
                
                <li className="page-item active">
                  <span className="page-link">
                    1
                    <span className="sr-only">(current)</span>
                  </span>
                </li>
                <li className="page-item"><Link to={link} onSelect={WindowScroll()} className="page-link">2 </Link></li>
                <li className="page-item"><Link to={link} onSelect={WindowScroll()} className="page-link">3</Link></li>
                <li className="page-item"><Link to={link} onSelect={WindowScroll()} className="page-link">4 </Link></li>
                <li className="page-item"><Link to={link} onSelect={WindowScroll()} className="page-link">5</Link></li>
                <li className="page-item"><Link to={link} onSelect={WindowScroll()} className="page-link">6</Link></li>
                <li className="page-item">
                  <Link to={link} onSelect={WindowScroll()} className="page-link" aria-label="Next">
                    <span aria-hidden="true">»</span>
                    <span className="sr-only">Next</span>
                  </Link>
                </li>
              </ul>
            </nav>
			
    <div className="tablet-table">
	  
		<InnerHeaderForMobile OpenConsoleTab={props.OpenConsoleTab} />

		<SearchPanelForMobile SetStartDate={props.SetStartDate} SetEndDate={props.SetEndDate} StartDate={props.StartDate} EndDate={props.EndDate} />
			
        <RequestTabDataForMobile />
                
              <div className="load-more-btn text-center">
                <Link to={link} onSelect={WindowScroll()} className="btn btn-primary">Load More</Link>
              </div>
            </div>
        </div>
      </div>
      </div>
      </div>
	);
}

const Content = (props) => {
  const [key, setKey] = useState(1);
  const [StartDateValue, SetStartDateValue] = useState(new Date());
  const [EndDateValue, SetEndDateValue] = useState(new Date());
	
	const SetStartDate = (value) => {
		SetStartDateValue(value);
	}
	
	const SetEndDate = (value) => {
		SetEndDateValue(value);
	}
	
	const OpenTab = (value) => {
		setKey(value);
	}
	
	if(props.AccessToken === "")
	{
		window.location.href = "/";
		
		return null;
	}
	else
	{
    return (
        <div className="main">
	
		
    <Tabs activeKey={key}>
		
      <Tab eventKey={1}>
		<ConsoleTab OpenRequestTab={OpenTab}/>
      </Tab>
    
      <Tab eventKey={2}>
		<RequestTab OpenConsoleTab={OpenTab} SetStartDate={SetStartDateValue} SetEndDate={SetEndDateValue} StartDate={StartDateValue} EndDate={EndDateValue}/>
      </Tab>

    </Tabs>
  </div>
    );
	}
}

const mapStateToProps = (state) => {
	return {
		AccessToken: state.accessToken
	};
};

export default connect(mapStateToProps, null)(Console);