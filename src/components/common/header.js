import React from 'react';
import Dropdown from 'react-bootstrap/Dropdown';

import { connect } from 'react-redux';
import { logout } from '../redux';

import { Link } from 'react-router-dom';
import WindowScroll from '../common/windowScroll';

const menuToggle = React.forwardRef(({ children, onClick }, ref) => (
  <Link 
  to="/console" 
  className="dropdown-toggle" 
  ref={ref} 
  onClick={(e) => {
    e.preventDefault();
    onClick(e);
  }}>
    {children}
  </Link>
));

const Header = (props) => {
  const link = props.Link;
    return (
	<>
	<ul className="nav flex-column sidebar">
		<li className="nav-item">
			<Link to={link} onSelect={WindowScroll()}>
				<img src="images/console-icon-1.png" alt="icon" />
			</Link>
		</li>
		<li className="nav-item">
			<Link to={link} onSelect={WindowScroll()}>
				<img src="images/console-icon-2.png" alt="icon" />
			</Link>
		</li>
		<li className="nav-item" >
			<Link to={link} onSelect={WindowScroll()}>
				<img src="images/console-icon-3.png" alt="icon" />
			</Link>
		</li>
		<li className="nav-item">
			<Link to={link} onSelect={WindowScroll()}>
				<img src="images/console-icon-4.png" alt="icon" />
			</Link>
		</li>
		<li className="nav-item">
			<Link to={link} onSelect={WindowScroll()}>
				<img src="images/console-icon-5.png" alt="icon" />
			</Link>
		</li>
		<li className="nav-item">
			<Link to={link} onSelect={WindowScroll()}>
				<img src="images/console-icon-6.png" alt="icon" />
			</Link>
		</li>
		<li className="nav-item">
			<Link to={link} onSelect={WindowScroll()}>
				<img src="images/console-icon-7.png" alt="icon" />
			</Link>
		</li>
		<li className="nav-item">
			<Link to={link} onSelect={WindowScroll()}>
				<img src="images/console-icon-8.png" alt="icon" />
			</Link>
		</li>
		<li className="nav-item">
			<Link to={link} onSelect={WindowScroll()}>
				<img src="images/console-icon-9.png" alt="icon" />
			</Link>
		</li>
		<li className="nav-item">
			<Link to={link} onSelect={WindowScroll()}>
				<img src="images/console-icon-10.png" alt="icon" />
			</Link>
		</li>
	</ul>
	
	<div className="main">
  <header className="header">
    <nav className="navbar navbar-static-top navbar-expand-lg navbar-desktop">
      <div className="container-fluid">
        
		
		
		<Link to={link} onSelect={WindowScroll()} className="navbar-brand">
          <img className="img-fluid" src="images/logo.svg" alt="logo" />
        </Link>
        <div className="add-listing">
          <div className="login">
            <div className="dropdown lang">
              <Dropdown>
                <Dropdown.Toggle as={menuToggle}>
                  <img className="fleg" src="images/flag.svg" alt="" /> En
                </Dropdown.Toggle>

                <Dropdown.Menu>
                  <Dropdown.Item><Link to={link} onSelect={WindowScroll()}>English</Link></Dropdown.Item>
                  <Dropdown.Item><Link to={link} onSelect={WindowScroll()}>简体中文</Link></Dropdown.Item>
                  <Dropdown.Item><Link to={link} onSelect={WindowScroll()}>French</Link></Dropdown.Item>
                  <Dropdown.Item><Link to={link} onSelect={WindowScroll()}>Bahasa Indonesia</Link></Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>
              
            </div>
            <div className="dropdown">
              <Dropdown>
                <Dropdown.Toggle as={menuToggle}>
                  <img src="images/user.svg" alt="" />Profile
                </Dropdown.Toggle>

                <Dropdown.Menu>
                  <Dropdown.Item><Link to={link} onSelect={WindowScroll()}>My Profile</Link></Dropdown.Item>
                  <Dropdown.Item><Link to={link} onClick={() => props.logout()}>Logout</Link></Dropdown.Item>
                </Dropdown.Menu>
              </Dropdown>
            </div>
          </div>
        </div>
      </div>
    </nav>
    <div className="mobile-header">
     <div className="container">
      <div className="d-flex header-wrapper">
        <Link onClick={() => { document.body.classList.toggle("hmenu"); }}>
          <img src="images/menu-icon.svg" alt=""/>
        </Link>
        <Link to={link} onSelect={WindowScroll()} className="navbar-brand">
          <img className="img-fluid" src="images/logo.svg" alt="logo" />
        </Link>
      </div>
     </div>
    </div>
  </header>
  </div>
  </>
    );
}

const mapDispatchToProps = (dispatch) => {
	return {
		logout: () => dispatch(logout())
	};
};

export default connect(null, mapDispatchToProps)(Header);