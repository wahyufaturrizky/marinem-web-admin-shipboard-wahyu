import React from "react";
import { Switch, Route, BrowserRouter as Router } from "react-router-dom";

import CreateNewOrder from "../createNewOrder/createNewOrder";
import LaunchOrder from "../launchOrder/launchOrder";
import Console from "../console/console";
import Login from "../login/login";

export default class AllRoutes extends React.Component {
  render = () => {
    return (
      <Router>
        <Switch>
          <Route exact path='/createNewOrder' component={CreateNewOrder} />
          <Route exact path='/launchOrder' component={LaunchOrder} />
          <Route exact path='/console' component={Console} />
          <Route exact path='/' component={Login} />
        </Switch>
      </Router>
    );
  };
}
