import { Link } from "react-router-dom";
import WindowScroll from "../common/windowScroll";
import { login, loginSuccess } from "../redux";
import { connect } from "react-redux";

const Login = (props) => {
  const link = "/";
  return (
    <div className='container'>
      <div className='login-wrapper text-center'>
        <Link to={link} onSelect={WindowScroll()} className='navbar-brand'>
          <img
            className='img-fluid'
            src='images/logo.svg'
            alt='logo'
            width='240px'
          />
        </Link>
        <div className='login-intro'>
          <h2>Sign in to your account</h2>
          <p>Please enter your username and password to sign in.</p>
        </div>
        <div className='login-form'>
          <div className='login-icon'>
            <img src='images/user-icon.png' alt='icon' />
          </div>
          <form onSubmit={onFormSubmit}>
            <div className='form-group text-left'>
              <label>User Name</label>
              <div className='input-field'>
                <img src='images/usericon.svg' alt='' />
                <input
                  type='text'
                  className='form-control'
                  id='uname'
                  placeholder='User Name'
                />
              </div>
            </div>
            <div className='form-group text-left'>
              <label>Password</label>
              <div className='input-field'>
                <img src='images/passwordicon.svg' alt='' />
                <input
                  type='password'
                  className='form-control'
                  id='password'
                  placeholder='Password'
                />
              </div>
            </div>
            <div className='form-group d-flex required'>{props.error}</div>
            <div className='form-group d-flex justify-content-end'>
              <Link to={link} onSelect={WindowScroll()}>
                I forgot my password
              </Link>
            </div>
            <div className='login-btn'>
              <button
                onClick={() =>
                  props.loginSuccess(
                    document.getElementById("uname").value,
                    document.getElementById("password").value
                  )
                }
                className='btn btn-primary'
              >
                Login
              </button>
            </div>
          </form>
        </div>
      </div>
    </div>
  );
};

const onFormSubmit = (e) => {
  e.preventDefault();
};

const mapStateToProps = (state) => {
  return {
    AccessToken: state.accessToken,
    error: state.errorMessage,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    login: () => dispatch(login()),
    loginSuccess: (userNameVal, passwordVal) =>
      dispatch(loginSuccess(userNameVal, passwordVal)),
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(Login);
