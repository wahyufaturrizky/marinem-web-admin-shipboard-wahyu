import { LOGIN, LOGOUT, LOGINError } from './loginTypes';

const initialState = {
	accessToken: getCookie("accessToken"),
	errorMessage: ""
}

const loginReducer = (state = initialState, action) => {
	switch(action.type) {
		case LOGIN: {
			setCookie("accessToken", action.payload);
			
			return {
				...state,
				accessToken: action.payload,
				errorMessage: ""
			};
		}
		case LOGOUT: {
			setCookie("accessToken", action.payload);
			
			return {
				...state,
				accessToken: action.payload,
				errorMessage: ""
			};
		}
		case LOGINError: {
			setCookie("accessToken", "");
			
			return {
				...state,
				accessToken: "",
				errorMessage: action.payload
			};
		}
		default: return state;
	}
};

export function setCookie(name, value)
{
	var time = new Date();
	
	time.setDate(time.getDate() + 1);
	
	document.cookie = name + "=" + value + ";expires=" + time.toGMTString() + ";path=/";
}

export function getCookie(name)
{
	var nameVal = name + "=";
	var decodedCookie = decodeURIComponent(document.cookie);
	var cookie = decodedCookie.split(";");
	
	for(var i=0; i<cookie.length; i++)
	{
		var c = cookie[0];
		
		while(c.charAt(0) === ' ')
		{
			c = c.substring(1);
		}
		if(c.indexOf(nameVal) === 0)
		{
			return c.substring(nameVal.length, c.length);
		}
	}
	
	return "";
}

export default loginReducer;