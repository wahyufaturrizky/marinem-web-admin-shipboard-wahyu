import { LOGIN, LOGOUT, LOGINError } from "./loginTypes";

export const login = (accessToken) => {
  return {
    type: LOGIN,
    payload: accessToken,
  };
};

export const loginError = (error) => {
  return {
    type: LOGINError,
    payload: error,
  };
};

export const logout = () => {
  window.location.href = "/";

  return {
    type: LOGOUT,
    payload: "",
  };
};

export const loginSuccess = (usernameVal, passwordVal) => {
  return (dispatch) => {
    if (usernameVal === "" && passwordVal === "") {
      dispatch(loginError("Enter user name and password!"));
      return;
    }

    if (usernameVal === "") {
      dispatch(loginError("Enter user name!"));
      return;
    }

    if (passwordVal === "") {
      dispatch(loginError("Enter password!"));
      return;
    }

    fetch(
      `${process.env.REACT_APP_URL_BE_TENANT1}/auth/oauth/token?grant_type=password&username=` +
        usernameVal +
        "&password=" +
        passwordVal +
        "&client_id=marinemClient",
      {
        method: "POST",
      }
    )
      .then((response) => {
        response.json().then((data) => {
          dispatch(login(data.access_token));

          window.location.href = "/console";

          return;
        });
      })
      .catch((error) => {
        dispatch(loginError("User name or password wrong!"));
        return;
      });
  };
};
